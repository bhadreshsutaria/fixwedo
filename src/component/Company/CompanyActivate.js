import React, {useEffect, useState} from "react";
import Modal from "react-bootstrap/Modal";
import {ApiService} from "../../ApiService";
import Login from "../Auth/Login/Login";

const CompanyActivate = () =>  {
    const [isLoginModal, setOpenLoginModal] = useState(true);
    const [isRegisterModal, setOpenRegisterModal] = useState(false);
    const [isShowForgotModal, setOpenForgotModal] = useState(false);
    const [Email, setEmail] = useState('');

    let apiService = new ApiService()

    const urlParam = new URLSearchParams(window.location.search)
    const id = urlParam.get('id')

    useEffect(() => {
        getUserDetails()
        activateCompanyAccount()

    },[]);

    const getUserDetails = async () => {
        const res = await apiService.getUserDetails(id)
        if(res.status){
            setEmail(res && res.data && res.data.Email)
        }
    }

    const activateCompanyAccount =async () => {
        const res = await apiService.activateCompanyAccount(id)
    }

    const handleLoginModal = (login, signup) => {
        setOpenLoginModal(login)
        setOpenRegisterModal(signup)
    }

    const handleForgot = () => {
        setOpenForgotModal(true)
        setOpenLoginModal(false)
    }

    return (
        <div>
            {
                isLoginModal && <Login Email={Email} isModalOpen={isLoginModal} onClose={handleLoginModal} onForgotPassword={handleForgot}/>
            }
        </div>
    );
}
export default CompanyActivate