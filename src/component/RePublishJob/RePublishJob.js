import React,{useEffect} from 'react'
import {ApiService} from "../../ApiService";
import {useParams} from "react-router";

const RePublishJob = () => {
    let apiService = new ApiService()
    const {id} = useParams();
    console.log(id)
    useEffect(() => {
        onRePublishJob()
    })
    const onRePublishJob = async ()  => {
        const data = await apiService.rePublishJob(id)
    }
    return(
        <div>
            Loading...
        </div>
    )
};
export default RePublishJob