import React from "react";
import Modal from 'react-bootstrap/Modal'
import Carousel from 'react-bootstrap/Carousel'
import ReadMoreAndLess from "react-read-more-less";
import images from "../../utils/ImageHelper";

export default function JobDetailsModal(props) {
    const img = (props.jobDetails && props.jobDetails.Files && props.jobDetails.Files.length)
    return (
        <Modal
            show={props.isOpenModal}
            onHide={() => props.handleModal()}
            size="lg"
            className="job-details-modal"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Body>
                {
                    img > 1 &&
                    <Carousel>
                        {
                            props.jobDetails && props.jobDetails.Files.map((img, ind) =>{
                                return(
                                    <Carousel.Item key={ind}>
                                        <img className="d-block w-100" src={img === "no_image" ?  props.jobDetails.type_work === "Electrician" ? images.Electrician : props.jobDetails.type_work === "Wood Worker" ? images.WoodWorker :  props.jobDetails.type_work === "Plumber" ? images.Plumbing : props.jobDetails.type_work === "Cleaner" ? images.Cleaner : props.jobDetails.type_work === "Gardener" ? images.Gardening : '' : img} alt={`First slide ${ind}`} />
                                    </Carousel.Item>
                                )
                            })
                        }
                    </Carousel>
                }
                {
                    img === 1 &&
                    <Carousel indicators={false} controls={false}>
                        <Carousel.Item>
                            <img className="d-block w-100" src={props.jobDetails && props.jobDetails.Files[0] === "no_image" ?  props.jobDetails.type_work === "El" ? images.Electrician : props.jobDetails.type_work === "Bygg" ? images.WoodWorker :  props.jobDetails.type_work === "VVS" ? images.Plumbing : props.jobDetails.type_work === "Städ" ? images.Cleaner : props.jobDetails.type_work === "Trädgård" ? images.Gardening : '' : props.jobDetails && props.jobDetails.Files[0]} alt={`First slide`}/>
                        </Carousel.Item>
                    </Carousel>
                }
                <div className="job-details-modal mt-5">
                <div className="content-box">
                    <h4>{props.jobDetails && props.jobDetails.type_work === "El" ? 'El' : props.jobDetails && props.jobDetails.type_work === "Bygg" ? 'Bygg' :  props.jobDetails && props.jobDetails.type_work === "VVS" ? 'VVS' : props.jobDetails && props.jobDetails.type_work === "Städ" ? 'Städ' : props.jobDetails && props.jobDetails.type_work === "Trädgård" ? 'Trädgård' : ''}</h4>
                    <div className={"job_discripation"}>
                        <div>
                            <span className="short-text">{props.jobDetails && props.jobDetails.description}</span>
                        </div>
                    </div>
                    {props.jobDetails && props.jobDetails.Location && (

                        props.loginUserId !==  props.jobDetails.lockedUserId ?
                            <h5 className={'blur-text infolocation'}><i
                                className="fas fa-map-marker-alt"/> xxxxxxxxxxxxxxxxxxxxxxxxx
                            </h5> :
                            <h5 className={` infolocation`}>
                                <i className="fas fa-map-marker-alt"/>
                                {props.jobDetails && props.jobDetails.Location}
                            </h5>
                    )}
                    {
                        props.loginUserId !== props.jobDetails.lockedUserId ?
                            <h5 className={'blur-text'}>xxxxxxxxxxxxxxxxxxxxxxxxx <span> ACTIVE </span>
                                <span className="hourbox">xxxx</span>
                            </h5> :
                            <h5>{props.jobDetails && props.jobDetails.nameOfUser} {props.jobDetails && props.jobDetails.IsActive && <span> ACTIVE </span>}
                                <span className="hourbox">{props.jobDetails && props.jobDetails.timeAgo}</span>
                            </h5>
                    }
                    {
                        props.loginUserId !== props.jobDetails.lockedUserId ?
                            <h5 className={'blur-text'}>xxxxxxxxxxxxxxxxxxxxxxxxx
                            </h5> :
                            <h5>{(props.jobDetails && props.jobDetails.email) || 'Test@gmail.com'}</h5>
                    }
                    {
                        props.loginUserId !== props.jobDetails.lockedUserId ?
                            <h5 className={'blur-text'}>xxxxxxxxxxxxxxxxxxxxxxxxx
                            </h5> : <h5>{(props.jobDetails && props.jobDetails.telephone) || "1234567890"}</h5>
                    }
                </div>
                </div>
            </Modal.Body>
        </Modal>
    );
}
