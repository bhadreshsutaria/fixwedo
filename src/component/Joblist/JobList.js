import React, {useEffect, useState} from "react";
import ReadMoreAndLess from "react-read-more-less";
import {ApiService} from "../../ApiService";
import Slider from "react-slick";
import images from "../../utils/ImageHelper";
import {Link, useHistory} from "react-router-dom";
import Login from "../Auth/Login/Login";
import CompanyRegister from "../Company/CompanyRegister";
import PaymentModal from "../Payment/PaymentModal";
import * as actions from "../../redux/action/actions";
import {toast} from "react-toastify";
import ForgotPassword from '../Auth/ForgotPassword'
import { Multiselect } from 'multiselect-react-dropdown';
import JobDetailsModal from "./JobDetailsModal";
import {baseUrl} from "../../utils/utility";
import CreatePassword from "../Auth/CreatePassword";

const JobList = () => {
    let apiService = new ApiService()
    const [jobList, setJobList] = useState([])
    const [isLoginModal, setOpenLoginModal] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [isRegisterModal, setOpenRegisterModal] = useState(false);
    const [isPaymentModalShow, setPaymentModalShow] = useState(false);
    const [isPaymentLoading, setPaymentLoading] = useState(false);
    const [activeProjectInfo, setActiveProjectInfo] = useState({});
    const [selecteddOption, setSelecteddOption] = useState([]);
    const [isShowForgotModal, setOpenForgotModal] = useState(false);
    const [isShowJobDetailsModal, setShowJobDetailsModal] = useState(false);
    const [isForgetPassword, setIsForgetPassword] = useState(false);
    const [selectedJob, setSelectedJob] = useState({});
    let history = useHistory();
    let loginUserId = localStorage.getItem('userId') || ''

    useEffect(() => {
        setIsLoading(true)
        getData()
        activateCompanyAccount()
    }, [])
    
    const activateCompanyAccount =async () => {
        const urlParam = new URLSearchParams(window.location.search)
        const id = urlParam.get('id')
        if(id){
            const res = await apiService.activateCompanyAccount(id)
        }
    }
    const getData = async () => {
       
        const response = await apiService.getJobList(loginUserId)
        if (response.status) {

            setJobList(response.data)
            setIsLoading(false)
        } else {
            setIsLoading(false)
        }
    }

    const onReadMore = (record) => {
        if(isShowJobDetailsModal === false){
            setSelectedJob(record)
            setShowJobDetailsModal(true)
        } else {
            setSelectedJob({})
            setShowJobDetailsModal(false)
        }

        //history.push(`/job-list/${record.id}`)
    }
    const settings = {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true
    };
    const onUnLockJob = async (record) => {
        if(loginUserId === ""){
            setOpenLoginModal(true)
        } else {
            setActiveProjectInfo(record)
            setPaymentModalShow(true)
        }
    }

    const handleLoginModal = (login, signup) => {
        const urlParam = new URLSearchParams(window.location.search)
        urlParam.delete('popup')
        urlParam.delete('id')
        history.replace({
          search: urlParam.toString(),
        })
        setOpenLoginModal(login)
        setOpenRegisterModal(signup)
    }

    const handleForgot = () => {
        setOpenForgotModal(true)
        setOpenLoginModal(false)
    }

    const handleLoginRegister = ( login, signup,) => {
        const urlParam = new URLSearchParams(window.location.search)
        urlParam.delete('popup')
        urlParam.delete('id')
        history.replace({
          search: urlParam.toString(),
        })
        setOpenLoginModal(login)
        setOpenRegisterModal(signup)
    }

    const handlePaymentSuccess = async (resData) =>{
        const payload = {
            ...activeProjectInfo,
            projectId: activeProjectInfo.id,
            lockedUserId: loginUserId,
            paymentId: resData.charge && resData.charge.id
        }
        const res = await apiService.paymentMethod(payload)
        if(res.status){
           getData()
            toast.success('Betalning genomförd')
        } else {
            toast.error('Payment not successfully')
        }
    }
    const handlePaymentModal = () =>{
        setPaymentModalShow(false)
    }
    const userInfo = JSON.parse(localStorage.getItem('userDetails')) || {}
    const userType = (userInfo && userInfo.UserType)

    const logout = async () => {
        const payload = {
           ...userInfo,
        }
        const data = await  apiService.logout(payload)
        if(data.status) {
            localStorage.removeItem("userId");
            localStorage.removeItem("userDetails");
            toast.success('Logout successfully')
            setTimeout(() => {
                window.location = baseUrl
            }, 1000)

        }
        /*firebase.collection("tblUser").doc(userInfo.id).set({
            ...userInfo,
            IsLogin: false,
        }).then((res) => {
            window.location.reload()
            localStorage.removeItem("userId");
            localStorage.removeItem("userDetails");
        }).catch((e) => {
        })*/
    }

    const onSelect = (selectedList, selectedItem) => {
        let clone = [...selecteddOption];
        clone.push(selectedItem);
        setSelecteddOption(clone)
    }

    const onRemove = (selectedList, removedItem) => {
        setSelecteddOption(selectedList)
    }

    const getAllStore = () => {
        let cloneJob = [...jobList]
        let dataItem = [];
        let storesData = cloneJob;
        if (selecteddOption && selecteddOption.length) {
            selecteddOption.map((y) => {
                storesData.map((x) => {
                    if(x.type_work === y.id){
                        dataItem.push(x)
                    }
                })
             //   dataItem = storesData.filter((x) => x.type_work === );
            })
        } else {
            dataItem = storesData;
        }
        return dataItem;
    };

    const handleCreatePassWord = () => {
        const urlParam = new URLSearchParams(window.location.search)
        urlParam.delete('popup')
        urlParam.delete('id')
        history.replace({
            search: urlParam.toString(),
        })
        setIsForgetPassword(false)
    }

    const urlParam = new URLSearchParams(window.location.search)
    const popup = urlParam.get('popup')
    const id = urlParam.get('id')
    if (popup == 'login' && isLoginModal === false) {
        setOpenLoginModal(true)
    }
    else if (popup == 'register' && isRegisterModal === false) {
        setOpenRegisterModal(true)
    } else if (popup == 'forget-password' && isForgetPassword === false) {

        setIsForgetPassword(true)
    }

    const storeDataList = getAllStore();
    return (
        <div id="wrapper" className="job-list-container">

            <div className="password_header">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-md-3 col-sm-4 col-8">
                            <div className="password_header_logo">
                                <a className="brand-logo" href='https://fixwedo.com/'>FixWeDo.</a>
                            </div>
                        </div>
                        <div className="col-md-9 col-sm-8 col-4 header-menu-col">
                            <div className="header_menu">
                            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                                <span className="navbar-toggler-icon"></span>
                                <span className="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse navbar-responsive-menu" id="navbarSupportedContent">
                                {
                                    userInfo.id ?
                                        <ul>
                                            <li><Link to={`${baseUrl}/company/dashboard`}>Välkommen {userInfo.FullName}</Link></li>
                                            <li><a onClick={logout}>Logga ut</a></li>
                                        </ul> :
                                        <ul>
                                            <li onClick={() => handleLoginModal(true, false)}><a>Logga in</a></li>
                                            <li onClick={() => handleLoginRegister(false, true)}><a>Anslut företag</a></li>
                                        </ul>
                                }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {
                isShowJobDetailsModal && <JobDetailsModal isOpenModal={isShowJobDetailsModal} handleModal={onReadMore} loginUserId={loginUserId} jobDetails={selectedJob}/>
            }
            {
                isLoginModal && <Login isModalOpen={isLoginModal} onClose={handleLoginModal} onForgotPassword={handleForgot}/>
            }
            {
                isRegisterModal && <CompanyRegister isRegisterModal={isRegisterModal} onClose={handleLoginRegister} />
            }
            {
                isShowForgotModal && <ForgotPassword open={isShowForgotModal} onClose={()=>setOpenForgotModal(false)} />
            }
            {
                isForgetPassword && <CreatePassword open={isForgetPassword} onClose={handleCreatePassWord} id={id}/>
            }
            {isPaymentModalShow &&
            <PaymentModal
                isPaymentLoading={isPaymentLoading}
                isPaymentModalShow={isPaymentModalShow}
                activeProjectInfo={activeProjectInfo}
                handlePaymentSuccess={handlePaymentSuccess}
                handlePaymentModal={handlePaymentModal}/>}
            <div id="page-wrapper" className={`menu-push job-list`}>
                <div className="row">
                    <div className="col col-md-6 col-sm-12 col-12 ">
                    <span className="joblist-section-title">Filtrera kategori</span>
                    <Multiselect
                        options={[{name: 'El', id: 'El'},{name: 'VVS', id: 'VVS'},{name: 'Trädgård', id: 'Trädgård'},{name: 'Bygg', id: 'Bygg'},{name: 'Städ', id: 'Städ'}]} // Options to display in the dropdown
                        selectedValues={selecteddOption} // Preselected value to persist in dropdown
                        onSelect={onSelect} // Function will trigger on select event
                        onRemove={onRemove} // Function will trigger on remove event
                        displayValue="name" // Property name to display in the dropdown options
                        placeholder="Välj"

                    />
                    </div>
                </div>
                <div className="container-fluid p-0">
                    {
                        isLoading ? <span>Läser in....</span> : <div className="row">
                            {storeDataList && storeDataList.length ?
                                (getAllStore() || []).map((item, ind) => {

                                    return (
                                        <div className="col-md-6">
                                            <div className="product_inner mt-0 mb-4 company_dashboard_main cd-ribbon active" key={ind}>
                                                <div className={`testimonials ${item.Files.length <= 1 ? "" : "multi-slide"}`} >
                                                    {
                                                        typeof(item.Files)  === "string" ? <div /*className="testimonial_box"*/ >
                                                            <a onClick={() => onReadMore(item)}><img src={ item.type_work === "El" ? images.Electrician : item.type_work === "Bygg" ? images.WoodWorker :  item.type_work === "VVS" ? images.Plumbing : item.type_work === "Städ" ? images.Cleaner : item.type_work === "Trädgård" ? images.Gardening : '' } className="img-fluid" /></a>
                                                        </div> : <Slider {...settings}>
                                                            {(item && item.Files || []).map((img) => (
                                                                <div /*className="testimonial_box"*/ key={img}>
                                                                    <a onClick={() => onReadMore(item)}><img src={img === "no_image" ?  item.type_work === "El" ? images.Electrician : item.type_work === "Bygg" ? images.WoodWorker :  item.type_work === "VVS" ? images.Plumbing : item.type_work === "Städ" ? images.Cleaner : item.type_work === "Trädgård" ? images.Gardening : '' : img} className="img-fluid" alt={img}/></a>
                                                                </div>
                                                            ))}
                                                        </Slider>
                                                    }

                                                </div>
                                                <div className="content-box">
                                                    <h4> {item.type_work === "El" ? 'El' : item.type_work === "Bygg" ? 'Bygg' :  item.type_work === "VVS" ? 'VVS' : item.type_work === "Städ" ? 'Städ' : item.type_work === "Trädgård" ? 'Trädgård' : ''} </h4>
                                                    <div className={"job_discripation"}>
                                                        <ReadMoreAndLess className="read-more-content" wordLimit={100}
                                                                         readMoreText=" Mer"
                                                                         readLessText=" Mindre">{item.description}</ReadMoreAndLess>
                                                    </div>
                                                    {item.address && (

                                                            <h5 className={`infolocation`}>
                                                                <i className="fas fa-map-marker-alt"/>
                                                                {item.address}
                                                            </h5>
                                                    )}
                                                    <h4>Kontaktuppgifter</h4>
                                                    {item.lockedUserId !== loginUserId ?
                                                        <h5 className={'blur-text'}>xxxxxxxxxxxxxxxxx<span> ACTIVE </span>
                                                            <span className="hourbox">xxxx</span>
                                                        </h5> :
                                                        <h5>{item.nameOfUser} {item.IsActive && <span> ACTIVE </span>}
                                                            <span className="hourbox">{item.timeAgo}</span>
                                                        </h5>
                                                    }
                                                    {loginUserId !== item.lockedUserId ?
                                                        <h5 className={'blur-text'}>xxxxxxxxxxxxxxxxx
                                                        </h5> : <h5>{(item && item.email) || 'Test@gmail.com'}</h5>
                                                    }
                                                    {loginUserId !== item.lockedUserId ?
                                                        <h5 className={'blur-text'}>xxxxxxxxxxxxxxxxx
                                                        </h5> : <h5>{(item && item.telephone) || "1234567890"}</h5>
                                                    }
                                                    {
                                                        item && item.count >= 3 ?  '' :  <a className={loginUserId === item.lockedUserId ? "unlocked" : "chatbox" } onClick={ loginUserId === item.lockedUserId ? () => onReadMore(item) : () => onUnLockJob(item)}>
                                                            {loginUserId === item.lockedUserId ? "Visa uppdrag" : "Lås upp" } <img src={images.lockimg} alt=""/>
                                                        </a>
                                                    }

                                                </div>
                                            </div>
                                        </div>
                                    )
                                }) :''
                            }
                        </div>
                    }

                </div>
            </div>
        </div>
    )
}
export default JobList