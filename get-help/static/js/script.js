//Bot pop-up intro
document.addEventListener('DOMContentLoaded', function() {
    var elemsTap = document.querySelector('.tap-target');
    var instancesTap = M.TapTarget.init(elemsTap, {});
    instancesTap.open();
    setTimeout(function() { instancesTap.close(); }, 4000);

});


//initialization
$(document).ready(function() {


    //Bot pop-up intro
    $("div").removeClass("tap-target-origin")

    //drop down menu for close, restart conversation & clear the chats.
    $('.dropdown-trigger').dropdown();

    showBotTyping();
    $("#userInput").prop('disabled', false);

    //global variables
    TRIGGER_INTENT = '/post_job';
    //URL = 'http://localhost:5005';
    URL = 'https://fixwedo.com:5009';

    var now = new Date();
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var day = now.getDate();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();

    user_id = "user_" + year.toString() + month.toString() + day.toString() + hour.toString() + minute.toString() + second.toString();

    action_trigger();



    google.maps.event.addDomListener(window, 'load', initialize);

})

function confirmLocation() {

    address = document.getElementById('address').innerText;
    lat = document.getElementById('lat').innerText;
    lng = document.getElementById('lng').innerText;
    pac_value = document.getElementById('searchTextField').value;
    // console.log(pac_value, lat, lng, address);
    if ((lat != '') && (lng != '') && (address != '') && (pac_value == address) && (Boolean(pac_value))) {
        send('/inform{"latitude" : "' + lat + '" , "longitude" : "' + lng + '" , "address" : "' + address + '"}');
        setUserResponse(address);
        $('#locationForm').remove();

    } else {
        alert('Unable to find valid address. Please try again');
        document.getElementById('address').innerText = '';
        document.getElementById('lng').innerText = '';
        document.getElementById('lat').innerText = '';
        document.getElementById('searchTextField').value = '';
        // $('#locationBtn').prop("disabled", true);
    }

}

function validateForm() {
    form = $("#validateform")[0];
    var name = form.name.value;
    var email = form.email.value;
    var telephone = form.telephone.value;
    address = document.getElementById('address').innerText;
    lat = document.getElementById('lat').innerText;
    lng = document.getElementById('lng').innerText;
    var type_of_work = form['type_of_work'].value;
    var description = form.description.value;
    pac_value = document.getElementById('searchTextField').value;

    // console.log(pac_value, lat, lng, address);
    if ((lat != '') && (lng != '') && (address != '') && (pac_value == address) && (Boolean(pac_value))) {
        msg = '/submit{"name":"' + name + '","email":"' + email + '","telephone":"' + telephone + '","address":"' + address + '","latitude":"' + lat + '","longitude":"' + lng + '","type_work":"' + type_of_work + '","description":"' + description + '"}';
        send(msg);
        $("#divValidateForm").remove();
        showBotTyping();
    } else {
        alert('Address is not Valid');
        document.getElementById('address').innerText = '';
        document.getElementById('lat').innerText = '';
        document.getElementById('lng').innerText = '';
        document.getElementById('searchTextField').value = '';
        // $("#divValidateForm").remove();

    }

}

function initialize() {
    // $('#locationBtn').prop("disabled", true);
    var input = document.getElementById('searchTextField');

    // var defaultBounds = new google.maps.LatLngBounds(
    //     new google.maps.LatLng(68.9, 9.26),
    //     new google.maps.LatLng(55, 26));
    //   var options = {
    //     bounds: defaultBounds,
    //     types: ['establishment']
    //   };
    var options = {
        types: ['(regions)'],
        componentRestrictions: { country: "se" }
    };

    // var autocomplete = new google.maps.places.Autocomplete(input);
    var autocomplete = new google.maps.places.Autocomplete(input, options);
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        const place = autocomplete.getPlace();

        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        } else {
            document.getElementById('searchTextField').type = 'text';
            // input.innerText = place.formatted_address;
            document.getElementById('lat').innerText = place.geometry.location.lat();
            document.getElementById('lng').innerText = place.geometry.location.lng();
            document.getElementById('address').innerText = place.formatted_address;
            document.getElementById('searchTextField').value = place.formatted_address;
            // $('#locationBtn').prop("disabled", false);
        }
    });
}



function setSlots() {
    form = $("#name_age")[0];
    var name = form.name.value;

    if (((/^([a-zA-Z][a-zA-Z ,.]*)$/.test(name)))) {
        $("#name").prop("disabled", true);
        $("#submit").prop("disabled", true);

        send(name);
        setUserResponse(name);

        $("#userInput").prop("disabled", false);
        $('#nameIDForm').remove();

    } else {
        alert('Name must contain Alphabets');
    }

}

function selectimage(event) {

    var files = event.target.files;
    var output = document.getElementById("result");

    for (var i = 0; i < files.length; i++) {
        var file = files[i];

        //Only pics
        if (!file.type.match('image'))
            continue;

        var picReader = new FileReader();

        picReader.addEventListener("load", function(event) {

            var picFile = event.target;
            // console.log(picFile.name);

            var div = document.createElement("div");

            div.innerHTML = "<img class='thumbnail' src='" + picFile.result + "'" +
                "title='" + picFile.name + "'/>";

            output.insertBefore(div, null);

        });

        //Read the image
        picReader.readAsDataURL(file);
    }

}

function uploadimage() {

    if ($('.thumbnail').length > 0) {
        $("#selectbtn").prop("disabled", true);
        $("#uploadbtn").prop("disabled", true);
        $("#nouploadbtn").prop("disabled", true);

        elements = $('.thumbnail');

        var firebaseConfig = {
            apiKey: 'AIzaSyB6J4Y-lNpNQ-ijDqoVs1fINklycWCtnA4',
            authDomain: 'agreeda.firebaseapp.com',
            databaseURL: 'https://agreeda.firebaseio.com',
            storageBucket: 'agreeda.appspot.com'
        };

        firebase.initializeApp(firebaseConfig);

        msg = '';
        for (i = 0; i < elements.length; i++) {

            var now = new Date();
            var year = now.getFullYear();
            var month = now.getMonth() + 1;
            var day = now.getDate();
            var hour = now.getHours();
            var minute = now.getMinutes();
            var second = now.getSeconds();

            image_name = "image_" + year.toString() + month.toString() + day.toString() + hour.toString() + minute.toString() + second.toString() + '_' + i.toString();
            var storage = firebase.storage();
            var storageRef = storage.ref('image/' + image_name);
            var imageData = elements[i].src;
            // console.log(imageData);
            msg += 'https://firebasestorage.googleapis.com/v0/b/agreeda.appspot.com/o/image%2F' + image_name + '?alt=media' + '\n\n';

            imagetask = storageRef.putString(imageData, 'data_url');

        }
        send(msg);
        setUserResponse($('.thumbnail').length + ' bild har laddats upp');

        $('#imageUploadForm').remove();
        $("#selectbtn").prop("disabled", true);
        $("#uploadbtn").prop("disabled", true);


    } else {
        send('no_image');
        setUserResponse('Jag har inga bilder');
        $('#imageUploadForm').remove();
        $("#selectbtn").prop("disabled", true);
        $("#uploadbtn").prop("disabled", true);
    }
}

function skipImage() {
    $("#selectbtn").prop("disabled", true);
    $("#uploadbtn").prop("disabled", true);
    $("#nouploadbtn").prop("disabled", true);
    send('no_images');
    setUserResponse('No bild har laddats upp');
    $('#imageUploadForm').remove();
}


// ========================== restart conversation ========================
function restartConversation() {
    $("#userInput").prop('disabled', true);
    //destroy the existing chart
    $('.collapsible').remove();

    if (typeof chatChart !== 'undefined') { chatChart.destroy(); }

    $(".chart-container").remove();
    if (typeof modalChart !== 'undefined') { modalChart.destroy(); }
    $(".chats").html("");
    $(".usrInput").val("");
    send("/restart");
}

// ========================== let the bot start the conversation ========================
function action_trigger() {

    send(TRIGGER_INTENT);

}

function keypress() {

    $("#name").on("keyup keypress", function(e) {
        var keyCode = e.keyCode || e.which;
        var text = $("#name").val();
        if (keyCode === 13) {

            if (text == "" || $.trim(text) == "") {
                e.preventDefault();
                return false;
            } else {
                setSlots();
            }
        }



    });

}
//===================================== user enter or sends the message =====================
$(".usrInput").on("keyup keypress", function(e) {
    var keyCode = e.keyCode || e.which;

    var text = $(".usrInput").val();
    if (keyCode === 13) {

        if (text == "" || $.trim(text) == "") {
            e.preventDefault();
            return false;
        } else {
            //destroy the existing chart, if yu are not using charts, then comment the below lines
            $('.collapsible').remove();
            if (typeof chatChart !== 'undefined') { chatChart.destroy(); }

            $(".chart-container").remove();
            if (typeof modalChart !== 'undefined') { modalChart.destroy(); }



            $("#paginated_cards").remove();
            $(".suggestions").remove();
            $(".quickReplies").remove();
            $(".usrInput").blur();
            setUserResponse(text);
            send(text);
            e.preventDefault();
            return false;
        }
    }
});

$("#sendButton").on("click", function(e) {
    var text = $(".usrInput").val();
    if (text == "" || $.trim(text) == "") {
        e.preventDefault();
        return false;
    } else {
        //destroy the existing chart

        if (typeof chatChart !== "undefined") {
            chatChart.destroy();
        }
        $(".chart-container").remove();
        if (typeof modalChart !== 'undefined') { modalChart.destroy(); }

        $(".suggestions").remove();
        $("#paginated_cards").remove();
        $(".quickReplies").remove();
        $(".usrInput").blur();
        setUserResponse(text);
        send(text);
        e.preventDefault();
        return false;
    }
})

//==================================== Set user response =====================================
function setUserResponse(message) {
    var UserResponse = '<img class="userAvatar" src=' + "./static/img/userAvatar.jpg" + '><p class="userMsg">' + message + ' </p><div class="clearfix"></div>';
    $(UserResponse).appendTo(".chats").show("slow");

    $(".usrInput").val("");
    scrollToBottomOfResults();
    showBotTyping();
    $(".suggestions").remove();
}

//=========== Scroll to the bottom of the chats after new message has been added to chat ======
function scrollToBottomOfResults() {

    var terminalResultsDiv = document.getElementById("chats");
    terminalResultsDiv.scrollTop = terminalResultsDiv.scrollHeight;
}

//============== send the user message to rasa server =============================================
function send(message) {

    $.ajax({
        url: URL + "/webhooks/rest/webhook",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({ message: message, sender: user_id }),
        success: function(botResponse, status) {
            //console.log("Response from Rasa: ", botResponse, "\nStatus: ", status);

            // if user wants to restart the chat and clear the existing chat contents
            if (message.toLowerCase() == '/restart') {
                $("#userInput").prop('disabled', false);

                //if you want the bot to start the conversation after restart
                action_trigger();
                return;
            }
            setBotResponse(botResponse);

        },
        error: function(xhr, textStatus, errorThrown) {

            if (message.toLowerCase() == '/restart') {
                $("#userInput").prop('disabled', false);

                //if you want the bot to start the conversation after the restart action.
                action_trigger();
                return;
            }

            // if there is no response from rasa server
            setBotResponse("");
            // console.log("Error from bot end: ", textStatus);
        }
    });
}

function addBotResponse(text) {
    var BotResponse = '<img class="botAvatar" src="./static/img/sara_avatar.png"/><p class="botMsg">' + text + '</p><div class="clearfix"></div>';
    $(BotResponse).appendTo(".chats").hide().fadeIn(1000);
}

//=================== set bot response in the chats ===========================================
function setBotResponse(response) {

    //display bot response after 500 milliseconds
    setTimeout(function() {
        hideBotTyping();
        if (response.length < 1) {
            //if there is no response from Rasa, send  fallback message to the user
            var fallbackMsg = "I am facing some issues, please try again later!!!";

            var BotResponse = '<img class="botAvatar" src="./static/img/sara_avatar.png"/><p class="botMsg">' + fallbackMsg + '</p><div class="clearfix"></div>';

            $(BotResponse).appendTo(".chats").hide().fadeIn(1000);
            scrollToBottomOfResults();
        } else {

            //if we get response from Rasa
            for (i = 0; i < response.length; i++) {

                //check if the response contains "text"
                if (response[i].hasOwnProperty("text")) {
                    $("#userInput").prop('disabled', false);
                    var BotResponse = '<img class="botAvatar" src="./static/img/sara_avatar.png"/><p class="botMsg">' + response[i].text + '</p><div class="clearfix"></div>';
                    $(BotResponse).appendTo(".chats").hide().fadeIn(1000);
                }

                //check if the response contains "images"
                if (response[i].hasOwnProperty("image")) {
                    $("#userInput").prop('disabled', false);
                    var BotResponse = '<div class="singleCard">' + '<img class="imgcard" src="' + response[i].image + '">' + '</div><div class="clearfix">';
                    $(BotResponse).appendTo(".chats").hide().fadeIn(1000);
                }


                //check if the response contains "buttons" 
                if (response[i].hasOwnProperty("buttons")) {
                    addSuggestion(response[i].buttons);
                }

                //check if the response contains "attachment" 
                if (response[i].hasOwnProperty("attachment")) {

                    //check if the attachment type is "video"
                    if (response[i].attachment.type == "video") {
                        video_url = response[i].attachment.payload.src;

                        var BotResponse = '<div class="video-container"> <iframe src="' + video_url + '" frameborder="0" allowfullscreen></iframe> </div>'
                        $(BotResponse).appendTo(".chats").hide().fadeIn(1000);
                    }

                }
                //check if the response contains "custom" message  
                if (response[i].hasOwnProperty("custom")) {


                    //check if the custom payload type is "dropDown"
                    if (response[i].custom.payload == "dropDown") {
                        dropDownData = response[i].custom.data;
                        $("#userInput").prop('disabled', true);
                        //console.log('Disable')
                        renderDropDwon(dropDownData);
                        return;
                    }

                    //check if the custom payload type is "location"
                    if (response[i].custom.payload == "location") {
                        $("#userInput").prop('disabled', true);
                        var BotResponse = '<div id="locationForm"><img class="botAvatar" src="./static/img/sara_avatar.png"/><div class="botMsg">' +
                            '<input id="searchTextField" type="number" size="50" placeholder="Ange adress" autocomplete="on" runat="server" /><br>' +
                            '<button id= "locationBtn" onclick="confirmLocation()">Bekräfta adress</button>' +
                            '<p id="lat" style="display:none"></p>' +
                            '<p id="lng" style="display:none"></p>' +
                            '<p id="address" style="display:none"></p>' +
                            '</div><div class="clearfix"></div></div>';
                        $(BotResponse).appendTo(".chats").hide().fadeIn(1000);
                        initialize();
                        scrollToBottomOfResults();
                        return;
                    }

                    if (response[i].custom.payload == "uploadImg") {
                        $("#userInput").prop('disabled', true);
                        var BotResponse = '<div id="imageUploadForm"><img class="botAvatar" src="./static/img/sara_avatar.png"/><div class="botMsg">' +
                            '<button id="selectbtn" onclick="document.getElementById(\'files\').click()">Välj bilder</button><br>' +
                            '<output id="result" /><input id="files" type="file" accept="image/*" multiple style="display:none" onchange="selectimage(event)"/><button id="uploadbtn" onclick="uploadimage()">Nästa</button>' +
                            '</div><div class="clearfix"></div></div>';
                        $(BotResponse).appendTo(".chats").hide().fadeIn(1000);
                        keypress();
                    }
                    if (response[i].custom.payload == "name") {
                        var BotResponse = '<div id="nameIDForm"><img class="botAvatar" src="static/img/botAvatar.png"/><div class="botMsg"><form action = "" name="name_age" id="name_age"> <label for="name" class="active">Vänligen ange ditt namn?</label><input type="text" name="name" id="name" onkeydown="return (event.keyCode!=13);"><input type="button" id = "submit" name="submit" value="Bekräfta & gå vidare" onclick="setSlots()"></form></div><div class="clearfix"></div></div>';
                        $("#userInput").prop('disabled', true);
                        $(BotResponse).appendTo(".chats").hide().fadeIn(1000);
                    }
                    if (response[i].custom.payload == "form_submit") {

                        data = response[i].custom.data[0];
                        // console.log(data);

                        var name = data.name;
                        var email = data.email;
                        var telephone = data.telephone;
                        var address = data.address;
                        var description = data.description;
                        var type_of_work = data['type_work'];
                        var latitude = data.latitude;
                        var longitude = data.longitude;


                        var BotResponse = '<div id="divValidateForm"><img class="botAvatar" src="static/img/botAvatar.png"/><div class="botMsg"><form action="" method="get" id="validateform" name="validateform" >' +
                            '<label for="name">Namn</label><br><input type="text" id="name" name="name" value = "' + name + '" required><br>' +
                            '<label for="email">Email</label><br><input type="email" id="email" name="email" value = "' + email + '" required><br>' +
                            '<label for="telephone">Telefon</label><br><input type="tel" id="telephone" name="telephone" value = "' + telephone + '" required><br>' +
                            '<label for="location">Adress</label><br><input id="searchTextField" type="text" size="50" placeholder="Enter a location" autocomplete="off" runat="server" class="pac-target-input" value = "' + address + '" required><br>' +
                            '<label for="type_of_work">Kategori</label><br><select id="type_of_work" name="type_of_work" required><option value="' + type_of_work + '" selected disabled hidden>' + type_of_work + '</option><option value="Electrician">El</option><option value="Plumber">VVS</option><option value="Gardener">Trädgård</option><option value="Wood Worker">Bygg</option><option value="Cleaner">Städ</option></select><br>' +
                            '<label for="description">Beskrivning</label><br><textarea id="description" name="description" rows="10" cols="30" required>' + description + '</textarea><br>' +
                            '<input type="button" id = "submit" name="submit" value="Bekräfta" onclick="validateForm()"></form>' +
                            '<p id="lat" style="display:none">' + latitude + '</p>' +
                            '<p id="lng" style="display:none">' + longitude + '</p>' +
                            '<p id="address" style="display:none">' + address + '</p>' +
                            '</div><div class="clearfix"></div></div>';
                        $("#userInput").prop('disabled', true);
                        $(BotResponse).appendTo(".chats").hide().fadeIn(1000);
                        initialize();
                    }
                }
            }
            scrollToBottomOfResults();
        }
    }, 500);
}

//====================================== Toggle chatbot =======================================
// $("#profile_div").click(function() {
//     $(".profile_div").toggle();
//     $(".widget").toggle();
// });
//====================================== Toggle Menu =======================================
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, options);
});
// Or with jQuery
$(document).ready(function(){
    $('.sidenav').sidenav();
});





//====================================== DropDown ==================================================
//render the dropdown messageand handle user selection
function renderDropDwon(data) {
    var options = "";
    for (i = 0; i < data.length; i++) {
        options += '<option value="' + data[i].value + '">' + data[i].label + '</option>'
    }
    var select = '<div class="dropDownMsg"><select class="browser-default dropDownSelect"> <option value="" disabled selected>Välj kategori</option>' + options + '</select></div>'
    $(".chats").append(select);
    scrollToBottomOfResults();

    //add event handler if user selects a option.
    $("select").change(function() {
        var value = ""
        var label = ""
        $("select option:selected").each(function() {
            label += $(this).text();
            value += $(this).val();
        });

        setUserResponse(label);
        //send(value)
        send(label);
        $(".dropDownMsg").remove();
    });


}

//====================================== Suggestions ===========================================

function addSuggestion(textToAdd) {
    setTimeout(function() {
        var suggestions = textToAdd;
        var suggLength = textToAdd.length;
        $(' <div class="singleCard"> <div class="suggestions"><div class="menu"></div></div></diV>').appendTo(".chats").hide().fadeIn(1000);
        // Loop through suggestions
        for (i = 0; i < suggLength; i++) {
            $('<div class="menuChips" data-payload=\'' + (suggestions[i].payload) + '\'>' + suggestions[i].title + "</div>").appendTo(".menu");
        }
        scrollToBottomOfResults();
    }, 1000);
}

// on click of suggestions, get the value and send to rasa
$(document).on("click", ".menu .menuChips", function() {
    var text = this.innerText;
    var payload = this.getAttribute('data-payload');
    // console.log("payload: ", this.getAttribute('data-payload'))
    setUserResponse(text);
    send(payload);

    //delete the suggestions once user click on it
    $(".suggestions").remove();

});

//====================================== functions for drop-down menu of the bot  =========================================

//restart function to restart the conversation.
$("#restart").click(function() {
    restartConversation()
});

//clear function to clear the chat contents of the widget.
$("#clear").click(function() {
    $(".chats").fadeOut("normal", function() {
        $(".chats").html("");
        $(".chats").fadeIn();
    });
});

//close function to close the widget.
// $("#close").click(function() {
//     $(".profile_div").toggle();
//     $(".widget").toggle();
//     scrollToBottomOfResults();
// });



//======================================bot typing animation ======================================
function showBotTyping() {

    var botTyping = '<img class="botAvatar" id="botAvatar" src="./static/img/sara_avatar.png"/><div class="botTyping">' + '<div class="bounce1"></div>' + '<div class="bounce2"></div>' + '<div class="bounce3"></div>' + '</div>'
    $(botTyping).appendTo(".chats");
    $('.botTyping').show();
    scrollToBottomOfResults();
}

function hideBotTyping() {
    $('#botAvatar').remove();
    $('.botTyping').remove();
}
