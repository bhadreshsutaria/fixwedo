import geopy
from geopy.geocoders import Nominatim
from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker, FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.types import DomainDict
from rasa_sdk.events import SlotSet, AllSlotsReset, FollowupAction
import smtplib
from email.mime.text import MIMEText

from recognizers_text import Culture
from recognizers_sequence import recognize_email, recognize_phone_number

from datetime import datetime
from random import randint
import firebase_admin
from firebase_admin import credentials, firestore

cred = credentials.Certificate("actions/agreeda-firebase-adminsdk-hfnpd-7229029c08.json")
firebase_admin.initialize_app(cred, {"databaseURL": "https://Agreeda.firebaseio.com/"})
db = firestore.client()
doc_ref = db.collection(u'tblProjectMaster')

class ValidateLeadForm(FormValidationAction):
    
    def name(self) -> Text:
        return "validate_lead_form"

    def validate_email(
        self,
        value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:

        results = recognize_email(value, Culture.English)

        if len(results) > 0:
            result = results[0]
            value = result.resolution['value']

            return {'email': value}
        else:
            dispatcher.utter_message("Oops! Det där verkar inte vara en giltig emailadress.")
            return {'email': None}

    def validate_telephone(
        self,
        value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:

        results = recognize_phone_number(value, Culture.English)

        if len(results) > 0:
            result = results[0]
            value = result.resolution['value']

            return {'telephone': value}
        else:
            dispatcher.utter_message("Oops! Det där verkar inte vara en giltig telefonnummer.")
            return {'telephone': None}


class ActionFormComplete(Action):

    def name(self) -> Text:
        return "action_form_complete"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        name = tracker.get_slot('name')
        address = tracker.get_slot('address')
        description = tracker.get_slot('description')
        email = tracker.get_slot('email')
        telephone = tracker.get_slot('telephone')
        type_work = tracker.get_slot('type_work')
        latitude = tracker.get_slot("latitude")
        longitude = tracker.get_slot("longitude")

        message={"payload":"form_submit", 
                "data": [{
          "type_work": type_work,
          "description": description,
          "address": address,
          "name": name,
          "email": email,
          "telephone": telephone,
          "latitude": latitude,
          "longitude": longitude
          }
                ]
        }
        dispatcher.utter_message("Vänligen kontrollera dina uppgifter",json_message=message)

        return []


class ActionSubmit(Action):

    def name(self) -> Text:
        return "action_submit"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        name = tracker.get_slot('name')
        address = tracker.get_slot('address')
        description = tracker.get_slot('description')
        email = tracker.get_slot('email')
        telephone = tracker.get_slot('telephone')
        type_work = tracker.get_slot('type_work')
        image = tracker.get_slot('image').strip()

        flag = 0

        message = 'Following are the errors occured during validation: <br>'

        if len(name) < 1:
            message += 'Name must not be empty <br>'
            flag+=1

        if len(description) < 1:
            message += 'Description must not be empty <br>'
            flag+=1

        email_result = recognize_email(email, Culture.English)

        if len(email_result) > 0:
            result = email_result[0]
            email = result.resolution['value']

        else:
            message += 'Email is not Valid <br>'
            flag+=1

        telephone_result = recognize_phone_number(telephone, Culture.English)

        if len(telephone_result) > 0:
            result = telephone_result[0]
            telephone = result.resolution['value']

        else:
            message += 'Phone number is not valid <br>'
            flag+=1

        if flag == 0:
            return [SlotSet("name", name), SlotSet("description", description), SlotSet("email", email), SlotSet("telephone", telephone), FollowupAction('action_upload')]
        else:
            dispatcher.utter_message(message)
            return [FollowupAction('action_form_complete')]
    
class ActionUpload(Action):

    def name(self) -> Text:
        return "action_upload"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        name = tracker.get_slot('name')
        address = tracker.get_slot('address')
        description = tracker.get_slot('description')
        email = tracker.get_slot('email')
        telephone = tracker.get_slot('telephone')
        type_work = tracker.get_slot('type_work')
        latitude = tracker.get_slot("latitude")
        longitude = tracker.get_slot("longitude")
        postid = randint(100, 99999)

        image = tracker.get_slot('image').strip()

        if image !='no_images':
            print(image.split('\n\n'))
            image = image.split()
        else:
            image = [image]

        data = {
            'name': name,
            'email': email,
            'address': address,
            'description': description,
            'type_work': type_work,
            'telephone': telephone,
            "PostCode": '',
            "ProjectStatus" : '',
            "Title":type_work,
            "UpdatedBy": datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
            "createdDate": datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
            "weeklyReminder": False,
            "UpdatedDate": datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
            "isProjectLocked": False,
            "paymentId":'',
            "Files":image
        }

        doc_ref.add(data)

        dispatcher.utter_message(template='utter_form_complete')
        smtp_ssl_host = 'send.one.com'
        smtp_ssl_port = 465
        username = 'no-reply@fixwedo.com'
        password = 'noreply@123'
        sender = 'no-reply@fixwedo.com'
        targets = [email]
        bodyText = 'Hej {},<br />Tack för att du använder FixWeDo!<br /><br />Här kommer kontaktuppgifter till kund, samt uppgifter om uppdraget.<br /><br />Kontaktuppgifter till kund:<br /><b>Email:</b> {} <br /><b>Telefon:</b> {} <br /><b>Adress:</b> {} <br /> <br />Uppgifter om jobb: <br /><b>Kategori:</b> {} <br/><b>Beskrivning:</b> {} <br /><br />Du hittar även detta i din profil på FixWeDo.com.<br /><br />Kvittot på ditt köp skickas i ett separat mail.<br /><br />Vänliga hälsningar,<br />Teamet bakom FixWeDo'.format(name, email, telephone, address, type_work, description)

        msg = MIMEText(bodyText, 'html')
        msg['Subject'] = 'Tack! Ditt uppdrag har publicerats.'
        msg['From'] = sender
        msg['To'] = ', '.join(targets)

        server = smtplib.SMTP_SSL(smtp_ssl_host, smtp_ssl_port)
        server.login(username, password)
        server.sendmail(sender, targets, msg.as_string())
        server.quit()
