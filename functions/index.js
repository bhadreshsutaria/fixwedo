

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

// const stripe = require('stripe')(functions.config().keys.webhooks);
// const endpointSecret = functions.config().keys.signing;

const stripe = require("stripe")("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")

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
const express = require('express');
const nodemailer = require("nodemailer");
const requestIp = require('request-ip')
const axios = require('axios')
const crone = require('node-cron')
const bodyParser = require('body-parser')
const moment = require('moment')
const cors = require('cors')({ origin: true });
const app = express();

// const stripe = require('stripe')(functions.config().stripe.token);


function charge(req, res) {
     const body = JSON.parse(req.body);
     const amount = parseInt(body.amount);
     const currency = body.currency;
    stripe.tokens.create({
        card: {
            number: body.card.number,
            exp_month: body.card.exp_month,
            exp_year: body.card.exp_year,
            cvc: body.card.cvc,
        },
    }).then((data) => {
        stripe.charges.create({
            amount: amount,
            currency: currency,
            description: '',
            source: data.id,
            receipt_email: body.email
        }).then(charge => {
            send(res, 200, {
                message: 'Success',
                charge,
            });
        }).catch(err => {
            console.log(err);
            send(res, 500, {
                error: err.message,
            });
        });
    }).catch((er) => {
        send(res, 200, {
            message: er.message,
        });
    });

}

function send(res, code, body) {
    res.send({
        statusCode: code,
        body: body,
    });
}


app.use(bodyParser.json());
app.use(cors);

app.post('/', (req, res) => {
    // console.log(JSON.parse(req.body),'===');

    // Catch any unexpected errors to prevent crashing
    try {
        charge(req, res);
    } catch (e) {
        console.log(e);
        send(res, 500, {
            error: `The server received an unexpected error. Please try again and contact the site admin if the error persists.`,
        });
    }
});

app.post('/createStripeUser', (req, res) => {
    try {
        const body = JSON.parse(req.body);
        console.log("body=============>", body)
        stripe.customers.create({
            email: body.email,
        }).then(async (resData) => {
            const intent = await stripe.setupIntents.create({
                customer: resData.id,
            })
            console.log(intent)
            res.send({
                message: 'Success',
                resData,
                setup_secret: intent.client_secret
            }).catch(err => {
                console.log(err);
                res.send({
                    error: err.message,
                });
            });
        })
    } catch (e) {
        console.log(e);
        res.send({
            error: `The server received an unexpected error. Please try again and contact the site admin if the error persists.`,
        })
    }
});

app.post('/createPaymentMethod', (req, res) => {
    try {
        const body = JSON.parse(req.body);
        console.log("body=============>", body)
        if(body.email === ""){
            stripe.tokens.create({
                card: {
                    number: body.card.number,
                    exp_month: body.card.exp_month,
                    exp_year: body.card.exp_year,
                    cvc: body.card.cvc,
                },
            }).then(async (token, err) => {
                var tokenID = token.id;
                console.log(tokenID)
                console.log(err)
                await stripe.customers.createSource(body.CustomerId, {
                    source: tokenID
                }).then((resData) => {
                    res.send({
                        message: 'Success',
                        resData,
                    })
                }).catch(err => {
                    console.log(err);
                    res.send({
                        error: err.message,
                    });
                });
            })
        } else {
            stripe.customers.create({
                email: body.email,
            }).then((resData) => {
                stripe.tokens.create({
                    card: {
                        number: body.card.number,
                        exp_month: body.card.exp_month,
                        exp_year: body.card.exp_year,
                        cvc: body.card.cvc,
                    },
                }).then(async (token, err) => {
                    var tokenID = token.id;
                    console.log("========1=========>",tokenID)
                    console.log("========2=========>",err)
                    await stripe.customers.createSource(resData.id, {
                        source: tokenID
                    }).then((resData) => {
                        res.send({
                            message: 'Success',
                            resData,
                        })
                    }).catch(errs => {
                        console.log(errs);
                        console.log("========3=========>",errs)
                        res.send({
                            error: errs.message,
                        });
                    });
                })
            })
        }
    } catch (e) {
        console.log("========4=========>",e)
        res.send({
            error: `The server received an unexpected error. Please try again and contact the site admin if the error persists.`,
        })
    }
});

let transporter = nodemailer.createTransport({
    host: 'send.one.com',
    port: 465,
    secure: true, // use SSL
    auth: {
        user: 'no-reply@fixwedo.com',
        pass: 'noreply@123'
    }
});

exports.sendMail = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        const { dest, emailName, docId, projectTitle, name, message, messageLink, projectId, id } = req.query;
        const newMessageLink = `${messageLink}&projectId=${projectId}`
        const url = `${docId}?id=${id}&popup=login&email=${dest}`
        console.log("==========url============>",url)
        if (emailName === "newuserregister") {
            const mailOptions = {
                from: 'no-reply@fixwedo.com',
                to: dest,
                subject: "Activate account ✔",
                html: "<b>Hello " + dest + ",</b><br/><br/>" +
                    " Please <a href=" + url + " target=`_blank`>click here</a> to activate your account" + "<br/><br/>"
                    + "Thanks," + "<br/><b>FixWeDo.</b>", // html body
            };
            return transporter.sendMail(mailOptions, (erro, info) => {
                if (erro) {
                    console.log(erro.toString())
                    return res.send(erro.toString());
                }
                return res.send('Sended');
            });
        }else if(emailName === "ForgotPassword"){

            let linkUrl = `${docId}?popup=forget-password&id=${id}`
            console.log("docId=================>", linkUrl,  )
            console.log("id=================>", id,  )
            const mailOptions = {
                from: 'no-reply@fixwedo.com',
                to: dest,
                subject: "Forgot Password",
                html: "<b>Hello " + name + ",</b><br/><br/>" +
                    "As you requested for forget password." +
                    "<br/>" +
                    " Please <a href=" + linkUrl + " target=`_blank`>click here</a> to change your password." + "<br/><br/>"
                    + "Thanks," + "<br/><b>FixWeDo.</b>", // html body
            };
            return transporter.sendMail(mailOptions, (erro, info) => {
                if (erro) {
                    console.log(erro.toString())
                    return res.send(erro.toString());
                }
                return res.send('Sended');
            });
        }else if (emailName === "chat") {
            const mailOptions = {
                from: 'no-reply@fixwedo.com',
                to: dest,
                subject: String("You have unread messages for " + projectTitle + " from " + name),
                html: "You have unread messages for <b>" + projectTitle + "</b> from <b> " + name + " </b><br/><br/>" +
                    "<span>" + message + "</span>" +
                    "<br/>" +
                    "<br/>" +
                    "<a href="+ newMessageLink +" >Click here to replay.</a>" +
                    "<br/><br/>"
                    + "Thanks," + "<br/><b>FixWeDo.</b>", // html body
            };
            return transporter.sendMail(mailOptions, (erro, info) => {
                if (erro) {
                    console.log(erro.toString())
                    return res.send(erro.toString());
                }
                return res.send('Sended');
            });
        }else if(emailName === "newuserregistercompany"){

            const mailOptions = {
                from: 'no-reply@fixwedo.com',
                to: dest,
                subject: "Activate account ✔",
                html: "<b>Hej " + name + ",</b><br/><br/>" +
                    "</b>" +
                    " Välkommen till FixWeDo, vänligen <a href=" + url + " target=`_blank`>klicka här</a> för att aktivera ditt konto:" + "<br/><br/>"
                    + "Vänliga hälsningar," + "<br/><b>Teamet bakom FixWeDo</b>", // html body
            };
            return transporter.sendMail(mailOptions, (erro, info) => {
                if (erro) {
                    console.log("========erro=========>", erro)
                     res.send(erro.toString());
                } else {
                    console.log("========info=========>",info)
                     res.send('Sended');
                }

            });
        }else {
            const mailOptions = {
                from: 'no-reply@fixwedo.com',
                to: dest,
                subject: "Hello ✔",
                html: "<b>Hello world?</b>",
            };
            return transporter.sendMail(mailOptions, (erro, info) => {
                if (erro) {
                    console.log(erro.toString())
                    return res.send(erro.toString());
                }
                return res.send('Sended');
            });
        }
    });
});

app.get("/", (req, res) => {
    const clientIp = requestIp.getClientIp(req);
    console.log(clientIp)
    if (clientIp) {
        res.send({
            status: true,
            ipAddress: clientIp
        })
    } else {
        res.send({
            status: false,
            ipAddress: clientIp
        })
    }
})

exports.stripePayment = functions.https.onRequest(app);
exports.ipaddress = functions.https.onRequest(app);

app.get('/project-list',async (req, res) => {
    let userId = req.query.userId || ''
    let db = admin.firestore();
    const companyMaster = [];
    const counts = [];
   // console.log("=======userId=========>",userId)
    if(userId){
        await db.collection("tblCompanyProjectMaster").where('lockedUserId' ,'==', userId).get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => companyMaster.push({ ...doc.data(), id: doc.id }))
        })
    }
    await db.collection("tblCompanyProjectMaster").get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => counts.push({ ...doc.data(), id: doc.id }))
    });
   await db.collection("tblProjectMaster").orderBy("createdDate", "desc").get().then( (querySnapshot) => {
        let data = [];
        querySnapshot.forEach((doc) => data.push({ ...doc.data(), id: doc.id }));
        if(userId) {
            companyMaster.map( (y) => {
                const findindex = data.findIndex((x) => x.id === y.projectId)
                const onj = findindex === -1 ? {id: ""} : data[findindex]
                const count = counts.filter((x) => x.projectId === onj.id ) || []
                let obj = {...data[findindex], lockedUserId:  y.lockedUserId, count: count.length}
                data[findindex] = obj
            })
        } else {
            counts.map( (y) => {
                const findindex = data.findIndex((x) => x.id === y.projectId)
                const onj = findindex === -1 ? {id: ""} : data[findindex];
                let count = counts.filter((x) => x.projectId === onj.id );
                let obj = {...data[findindex], lockedUserId:  y.lockedUserId, count: count.length}
                data[findindex] = obj
            })
        }
       res.send({
           status: true,
           data: data,
       })

    }).catch(e => {
       console.log(e)
        res.send({
            status: false,
            data: e
        })
    });
})

app.get('/project-list/:id',(req, res) =>{
    let db = admin.firestore();
    let id = req.params.id;
    db.collection("tblCompanyProjectMaster").where('lockedUserId' ,'==', id).get().then((querySnapshot) =>{
        const data = [];
        querySnapshot.forEach((doc) => data.push({ ...doc.data(), id: doc.id }))
        db.collection("tblCompanyProjectMaster").orderBy("UpdatedBy", "desc").get().then((response) => {
            const subData = [];
            const finalData = []
            response.forEach((doc) => subData.push({ ...doc.data(), id: doc.id }))
            subData.map((x) => {
                data.map((y) => {
                    if(x.id === y.id){
                        finalData.push(x)
                    }
                })
            })
            res.send({
                status: true,
                data: finalData
            })
        })

    }).catch((e) => {
        res.send({
            status: false,
            data: [],
            message: e
        })
    })
})

app.get('/project-list-details/:id', async (req,res) => {
    let db = admin.firestore();
    let id = req.params.id;
    let userId = req.query.userId || ''
    console.log(userId)
    const companyMaster = [];
    const counts = [];
    if(userId){
    await db.collection("tblCompanyProjectMaster").where('lockedUserId' ,'==', userId).get().then((querySnapshot) => {
       querySnapshot.forEach((doc) => companyMaster.push({ ...doc.data(), id: doc.id }))
    })
    }
    await db.collection("tblCompanyProjectMaster").get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => counts.push({ ...doc.data(), id: doc.id }))
    });
   // console.log(companyMaster)
   await db.collection("tblProjectMaster").doc(id).get().then((querySnapshot) => {
        const data = querySnapshot.data();
        console.log(data)
        let finalData  = {};
        const index = companyMaster.findIndex((x) => x.projectId === id)
        console.log("index========>",index)
        let count = counts.filter((x) => x.projectId === id)
        if(index !== -1){
            finalData = {...data, lockedUserId: companyMaster[index].lockedUserId, count: count.length}
        }else {
            finalData = {...data,}
        }
        res.send({
            status: true,
            data: finalData,
            data1: companyMaster
        })
    }).catch(reason => {
        res.send({
            status: false,
            data: reason
        })
    });

})

app.post('/login', async (req, res) => {
    let db = admin.firestore();
    let bodyData = req.body;
    let clientIp = ''
    db.collection("tblUser").where("Email", "==", bodyData.emailId).where("Password", "==", bodyData.password).get().then((querySnapshot) => {
        if (!querySnapshot.empty) {
            querySnapshot.forEach( async(doc) => {
                const data = doc.data();
                if(data.IsActivated){
                    await axios.get('https://us-central1-agreeda.cloudfunctions.net/ipaddress')
                    .then(response => {
                        db.collection("tblUser").doc(doc.id).update({
                            IsLogin: true,
                            LoginIp:  response.data.ipAddress
                        }).then((suc) => {
                            res.send({
                                status: true,
                                data: {...data, IsLogin: true, LoginIp: response.data.ipAddress, id:doc.id}
                            })
                        }).catch((e) => {
                            res.send({
                                status: false,
                                data: e
                            })
                        })
                    })
                    .catch(error => {
                        console.log(error);
                    });
                } else{
                    res.send({
                        status: false,
                        message: "Ditt konto är ännu inte aktiverat. Vänligen kontrollera din email"
                    })
                }

            })
        } else {
            res.send({
                status: false,
                data:  'Du har angett felaktigt lösenord'
            })
        }
    }).catch(reason => {
        res.send({
            status: false,
            data: reason
        })
    });
})

app.post('/create-user', (req, res) => {
    let db = admin.firestore();
    let {emailId, PostCode, Password, FullName, OrganizationNumber, Address, CompanyName, telephone} = req.body;
    db.collection('tblUser').where("Email", "==", emailId).get().then((querySnapshot) => {
        if (querySnapshot.empty) {
            db.collection("tblUser").add({
                Email: emailId,
                UserName: emailId,
                PostCode: PostCode,
                Name: '',
                Password: Password,
                UserType: 'company',
                FullName: FullName,
                AboutUs: "",
                ProfileImage: "",
                OrganizationNo: OrganizationNumber,
                Address: Address,
                CompanyName: CompanyName,
                telephone: telephone,
                IsActivated: false,
                IsLogin: false,
                LoginIp: ""
            }).then((response) => {
                console.log(response.id)
                res.send({
                    status: true,
                    data: response.id
                })
            })
        } else {
            res.send({
                status: false,
                data: "Denna email är redan kopplat till ett konto. Logga in eller försök med en annan email."
            })
        }
    }).catch((e) => {
        res.send({
            status: false,
            data: e
        })
    })
})

app.post('/payment-method', async (req, res) => {
    let db = admin.firestore();
    let {Files, PostCode, ProjectStatus, Title, UpdatedBy, UpdatedDate, address, createdDate,
        description,email,id, isProjectLocked, lockedUserId, name, paymentId, projectId, telephone, type_work} = req.body;
    db.collection("tblCompanyProjectMaster").add({
        Files: Files,
        PostCode: PostCode,
        ProjectStatus: ProjectStatus,
        Title: Title,
        UpdatedBy: moment(new Date()).format("YYYY-MM-DD HH:mm:ss"),
        UpdatedDate: moment(new Date()).format("YYYY-MM-DD HH:mm:ss"),
        address: address,
        createdDate: createdDate,
        description: description,
        email: email,
        id: id || '',
        isProjectLocked: isProjectLocked,
        lockedUserId: lockedUserId,
        name: name,
        paymentId: paymentId,
        projectId: projectId,
        telephone: telephone,
        type_work: type_work
    }).then((response) => {
        db.collection("tblUser").doc(lockedUserId).get().then((querySnapshot) => {
            if (querySnapshot.exists) {
                const data = querySnapshot.data();
                let link = `http://fixwedo.com/job-list/${projectId}`
                const mailOptions = {
                    from: 'no-reply@fixwedo.com',
                    to: data.Email,
                    subject: "Ditt kvitto från FixWeDo",
                    html: "<b>Hej "+ data.FullName +"</b><br/><br/>" +
                        "<span>Tack för att du använder FixWeDo!</span><br/><br/>" +
                        "<span>Bifogat hittar du kvitto på ditt köp.</span><br/><br/>" +
                        "<span>Kontaktuppgifter och övriga detaljer om uppdraget hittar du i din profil</span><br/>" +
                        "<a href=" + link + " target=`_blank`>www.fixwedo.com</a><br/><br/><br/><br/><br/><br/>"
                };

                return transporter.sendMail(mailOptions, (erro, info) => {
                    if (erro) {
                        console.log(erro.toString())
                        return res.send({data: erro.toString(), status: true,});
                    }
                    res.send({
                        status: true,
                    })
                });
            } else {
                console.log("else")
            }
        })

    }).catch((e) => {
        res.send({
            status: false,
            data: e
        })
    })
})

app.get('/user-details/:id', (req, res) => {
    let db = admin.firestore();
    let id = req.params.id;
    db.collection("tblUser").doc(id).get().then((querySnapshot) => {
        if (querySnapshot.exists) {
            const data = querySnapshot.data();
            res.send({
                status: true,
                data: data
            })
        } else {
            console.log("else")
        }
    }).catch(reason => {
        res.send({
            status: false,
            data: reason
        })
    });
})

app.post('/update-user-details/:id', (req, res) => {
    let db = admin.firestore()
    let id = req.params.id;

    const {Email, Name, PostCode, telephone, UserName, FullName, AboutUs, ProfileImage, Password, UserType, OrganizationNo, Address, IsActivated, IsLogin, LoginIp, CompanyName} = req.body
    db.collection("tblUser").doc(id).set({
        Email, Name, PostCode, telephone, UserName, FullName, AboutUs, ProfileImage, Password, UserType, OrganizationNo, Address, IsActivated, IsLogin, LoginIp, CompanyName
    }).then((doc) => {
        res.send({
            status: true,
            data: 'Profile updated successfully'
        })
    }).catch((e) => {
        res.send({
            status: false,
            data: e
        })
    })

})

app.get('/card-list/:id', (req, res) => {
    let db = admin.firestore()
    let id = req.params.id;
    db.collection("tblPaymentMethods").where("UserId", "==", id).get().then((querySnapshot) => {
        if(!querySnapshot.empty){
            const data = [];
            querySnapshot.forEach((doc) => data.push({ ...doc.data(), id: doc.id }))
            res.send({
                status: true,
                data:  data
            })
        } else {
            res.send({
                status: true,
                data:  []
            })
        }
    }).catch(reason => {
        res.send({
            status: false,
            data: reason
        })
    });
})
app.get('/forgot-password-check-email/:email', (req, res) => {
    let db = admin.firestore()
    let email = req.params.email;

    db.collection('tblUser').where("Email", "==", email).get().then((querySnapshot) => {
        if(!querySnapshot.empty){
            querySnapshot.forEach((doc) => {
                res.send({
                    status: true,
                    data: {...doc.data(), id: doc.id}
                })
            })
        } else {
            res.send({
                status: false,
                data:  'Inget konto med denna email hittades'
            })
        }
    }).catch((e) => {
        res.send({
            status: false,
            data:  'Inget konto med denna email hittades'
        })
    })
   /* db.collection("tblUser").where("Email", "==", email).get().then((querySnapshot) => {
        if(!querySnapshot.empty){
            const data = [];
            querySnapshot.forEach((doc) => data.push({ ...doc.data(), id: doc.id }))
            res.send({
                status: true,
                data:  data
            })
        } else {
            res.send({
                status: true,
                data:  []
            })
        }
    }).catch(reason => {
        res.send({
            status: false,
            data: reason
        })
    });*/
})

app.post('/payment-method-create',  (req, res) => {
    let db = admin.firestore()
    const {email, type, card, CustomerId, userId, defaultCard} = req.body;
    db.collection("tblPaymentMethods").where("Email", "==", email).get().then(async(querySnapshot) => {
        if (!querySnapshot.empty) {
            const data = []
            querySnapshot.forEach((doc) => data.push({ ...doc.data(), id: doc.id }))
                stripe.tokens.create({
                    card: {
                        number: card.number,
                        exp_month: card.exp_month,
                        exp_year: card.exp_year,
                        cvc: card.cvc,
                    },
            }).then(async (token) => {
                var tokenID = token.id;
               // console.log('============tokenID======>',token)
                console.log('============CustomerId======>',data)
              //  console.log('============data======>',data)
             stripe.customers.createSource(data[0].CustomerId, {
                    source: tokenID
                }).then((resData) => {
                    console.log('===========resData=========>',resData)
                    db.collection("tblPaymentMethods").add({
                        UserId: userId,
                        CustomerId: data[0].CustomerId,
                        CardId: resData.id,
                        CreatedDate: new Date(),
                        Email: email,
                        isAddedCardDetails: false,
                        CardNumber: card.number.replace(/\s/g, ''),
                        Exp_month: card.exp_month,
                        Exp_year:  card.exp_year,
                        DefaultCard: defaultCard
                    }).then((responseData) => {
                        res.send({
                            status: true
                        })
                    })
                }).catch(err => {
                    console.log('========1===>',err)
                    res.send({
                        status: false,
                        error: err.message,
                    });
                });
            }).catch((e) => {
                res.send({
                    status: false,
                    error: e.message,
                });
                console.log('========2===>',e)
            })
        } else {
            console.log("=====hii=====>")
            stripe.customers.create({
                email: email,
            }).then((resData) => {
                console.log("==========res1Data=======>",resData)
                const cards = {
                       number: card.number,
                        exp_month: card.exp_month,
                        exp_year: card.exp_year,
                        cvc: card.cvc,
                }
                console.log("==========card=======>",cards)
                stripe.tokens.create({
                    card: {
                        number: card.number,
                        exp_month: card.exp_month,
                        exp_year: card.exp_year,
                        cvc: card.cvc,
                    },
                }).then(async (token, err) => {
                    var tokenID = token.id;
                    await stripe.customers.createSource(resData.id, {
                        source: tokenID
                    }).then((resData) => {

                        if(resData && resData.id && resData && resData.customer){
                            db.collection("tblPaymentMethods").add({
                                UserId: userId,
                                CustomerId: resData.customer,
                                CardId: resData.id,
                                CreatedDate: new Date(),
                                Email: email,
                                isAddedCardDetails: false,
                                CardNumber: card.number.replace(/\s/g, ''),
                                Exp_month: card.exp_month,
                                Exp_year: card.exp_year,
                                DefaultCard: defaultCard
                            }).then((r) => {
                                res.send({
                                    status: true,
                                })
                            }).catch((e) => {
                                console.log('========3===>',e)
                                res.send({
                                    status: false,
                                    error: e.message,
                                })
                            })
                        } else {
                            res.send({
                                status: false,

                            })
                        }
                    }).catch(errs => {
                        console.log('========4===>',errs)
                        res.send({
                            error: errs.message,
                        });
                    });
                })
            }).catch((e) => {
                console.log('========5===>',errs)
                res.send({
                    status: false,
                    error: e.message,
                })
            })
        }

    })

})

app.put('/default-card/:id/:userId',(req, res) => {
    let db = admin.firestore()
    let id = req.params.id;
    let userId = req.params.userId;
    const payload = req.body
    db.collection("tblPaymentMethods").where("UserId", "==", userId).get().then((querySnapshot) => {
        const data = [];
        querySnapshot.forEach((doc) => data.push({ ...doc.data(), id: doc.id }))
       // const finalData = data.filter(p => p.CardId !== payload.CardId)
        data.forEach((x, i) => {

            const payload = {
                CardId: x.CardId,
                UserId: x.UserId,
                CardNumber: x.CardNumber,
                Email: x.Email,
                Exp_month: x.Exp_month,
                Exp_year: x.Exp_year,
                CreatedDate: x.CreatedDate,
                CustomerId: x.CustomerId,
                DefaultCard: false,
                UpdatedDate: new Date()
            }

            db.collection("tblPaymentMethods").doc(x.id).update(payload).then((doc) => {
                console.log("=====i=====>",i)
            })
        })
        db.collection("tblPaymentMethods").doc(id).update(payload).then((doc) => {
            console.log("=====i=====>",true)
            res.send({
                status: true
            })
        })
    })


})

app.put ('/logout', (req, res) => {
    const {id} = req.body
    let db = admin.firestore();
        db.collection("tblUser").doc(id).update({
            IsLogin: false,
    }).then((ress) => {
        res.send({
            status: true
        })
    }).catch((e) => {
        res.send({
            data: e,
            status: false
        })
    })
})

app.put ('/activate-company-account/:id', (req, res) => {
    let id = req.params.id;
    let db = admin.firestore();
        db.collection("tblUser").doc(id).update({
            IsActivated: true,
    }).then((ress) => {
        res.send({
            status: true
        })
    }).catch((e) => {
        res.send({
            data: e,
            status: false
        })
    })
})

app.put ('/forget-password/:id', (req, res) => {
    let id = req.params.id;
    let db = admin.firestore();
    const payload = req.body
    db.collection("tblUser").doc(id).update({
            IsActivated: true,
        Password: payload.Password
    }).then((ress) => {
        res.send({
            status: true
        })
    }).catch((e) => {
        res.send({
            data: e,
            status: false
        })
    })
})

app.delete ('/remove-card/:id', (req, res) => {
    let id = req.params.id;
    let db = admin.firestore();
    db.collection("tblPaymentMethods").doc(id).delete().then((ress)=>{
        res.send({
            status: true
        })
    }).catch((e) => {
        res.send({
            status: false
        })
    })
});


app.put('/weekly-reminder/:id',(req, res) => {
    let id = req.params.id;
    let db = admin.firestore();
    db.collection("tblProjectMaster").doc(id).update({
        weeklyReminder: false,
    }).then((rs) => {
        res.send({
            status: true
        })
    }).catch((e) => {
        console.log(e)
        res.send({
            status: false
        })
    })
});

crone.schedule('59 23 * * * ', (req, res) => {
    let db = admin.firestore();
    db.collection("tblProjectMaster").where("weeklyReminder", "==", false).get().then((querySnapshot) => {
        const data = [];
        querySnapshot.forEach((doc) => data.push({...doc.data(), id: doc.id}))
        data.map((x) => {
            let currentTime = new Date();

            let Now_Time = moment(currentTime).format("YYYY-MM-DD HH:mm:ss")
            let minute = moment(Now_Time).diff(x.UpdatedBy, 'minutes')
            let hour = minute / 60;
            let days = hour / 24;
           /* console.log(Math.round(days))
            console.log(x.email)*/
            if (Math.round(days) === 7) {
                console.log(x.email)
                let link = `http://fixwedo.com/job-job-list/re-publish-job/${x.id}`;
                const mailOptions = {
                    from: 'no-reply@fixwedo.com',
                    to: x.email,
                    subject: "Ditt uppdrag på FixWeDo",
                    html: "<b>Hej " + x.name + "</b><br/>" +
                        "<p>Nu har det gått en vecka sen du publicerade ditt uppdrag på FixWeDo och ditt uppdrag visas inte längre mot  Vi hoppas att du har kommit i kontakt med ett företag som kan hjälpa dig med din förfrågan!</p>" +
                        "<p>Om du ännu inte har fått hjälp eller kommit i kontakt med en hantverkare kan du enkelt återpublicera ditt uppdrag till FixWeDo. Ditt uppdrag kommer då att ligga synligt mot företag i 7 dagar till.</p>" +
                        "<a href=" + link + " target=`_blank` style='background: #00b926;border-radius: 5px; color: #fff; font-size: 14px; line-height: 20px;cursor: pointer;text-transform: uppercase;text-decoration: none;transition: all .3s ease 0s;font-weight: 700;padding: 10px 20px; margin-top: 10px; margin-bottom: 10px'>Återpublicera uppdrag</a><br/>" +
                        "<p>Tack för att du använder FixWeDo!</p><br/><br/>" +
                        "<p>Vänliga hälsningar,</p><br/>" +
                        "<p>Teamet bakom FixWeDo</p>" // html body
                };
                    db.collection("tblProjectMaster").doc(x.id).update({
                        weeklyReminder: true,
                        UpdatedBy: moment(currentTime).format("YYYY-MM-DD HH:mm:ss")
                    });
                return transporter.sendMail(mailOptions, (erro, info) => {
                    if (erro) {
                        console.log("===========>", erro.toString())
                        res.send({data: erro.toString(), status: true,});
                    }
                    res.send({
                        status: true,
                    })

                });
            }

        });
    });


});



exports.api = functions.https.onRequest(app);
